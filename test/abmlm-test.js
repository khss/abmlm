const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Abmlm", function () {
  it("Correct owner address", async function () {
	const [owner] = await ethers.getSigners();

    const Abmlm = await ethers.getContractFactory("Abmlm");
    const abmlmContract = await Abmlm.deploy();
    await abmlmContract.deployed();

    expect(await abmlmContract.getOwnerAddress()).to.equal(owner.address);
  });

  it("Plan A 2 levels" , async function() {
	const [owner, player1, player2] = await ethers.getSigners();

    const Abmlm = await ethers.getContractFactory("Abmlm");
    const abmlmContract = await Abmlm.deploy();
    await abmlmContract.deployed();

	// Player 1 is downline of Owner
	await abmlmContract.connect(player1).joinPlanA(owner.address);
	expect(await abmlmContract.getPlayerBalanceAmount(owner.address)).to.equal(100);

	// Player 2 is downline of Player1
	await abmlmContract.connect(player2).joinPlanA(player1.address);
	expect(await abmlmContract.getPlayerBalanceAmount(owner.address)).to.equal(110);
	expect(await abmlmContract.getPlayerBalanceAmount(player1.address)).to.equal(90);
  });

  it("Plan B level 0" , async function() {
	const [owner, player1, player2, player3] = await ethers.getSigners();

    const Abmlm = await ethers.getContractFactory("Abmlm");
    const abmlmContract = await Abmlm.deploy();
    await abmlmContract.deployed();

	// Player 1,2,3 is downline of Owner
	await abmlmContract.connect(player1).joinPlanA(owner.address);
	await abmlmContract.connect(player2).joinPlanA(owner.address);
	await abmlmContract.connect(player3).joinPlanA(owner.address);
	// After player 3, owner will join plan B
	expect(await abmlmContract.getPlanBPlayerAddressByIndexPosition(0)).to.equal(owner.address);
  });
});
