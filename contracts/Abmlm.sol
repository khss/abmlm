// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;

/**
 * @title AB MLM Contract
 * @dev Manage AB MLM processes
 */
contract Abmlm {
    struct PlayTransaction {
        uint256 txTime;
        address fromPlayer;
        address toPlayer;
        uint256 amount;
    }

    struct Player {
        bool isInit;
        uint256 balanceAmt;
        address uplineAddr;
        address[] downlineAddrList;
        uint256 planBCurrentIndexPos;
        uint256[] playTxIndexList;
    }

    address private ownerAddr;
    mapping (address => Player) private playerList;
    address[] private planBPlayerAddrList;

    // Play transaction list
    PlayTransaction[] playTxList;

    struct PlanBRecipient {
        bool isInit;
        uint256 indexPos;
        uint8 earnCount;
    }
    PlanBRecipient private planBRcp;

    // Config
    uint8 private earnCountThreshold;
    uint8 private ownerEarnAmount;
    uint8 private uplineEarnAmount;

    constructor() {                  
        earnCountThreshold = 3;
        ownerEarnAmount = 10;
        uplineEarnAmount = 90;

        ownerAddr = msg.sender;
        playerList[msg.sender].isInit = true;
    }

    function joinPlanA(address _uplineAddr) public {
        require(!playerList[msg.sender].isInit, "Joined previously!");
        require(playerList[_uplineAddr].isInit, "Invalid upline addr");
        require(playerList[_uplineAddr].downlineAddrList.length <= earnCountThreshold, "Max downline reached");

        // Insert player list
        playerList[msg.sender].isInit = true;
        playerList[msg.sender].uplineAddr = _uplineAddr;

        // Update the downline addr to the upline
        playerList[_uplineAddr].downlineAddrList.push(msg.sender);

        if(playerList[_uplineAddr].downlineAddrList.length < earnCountThreshold){
            // Insert the transactions if still within earn count threshold
            insertTx(msg.sender, ownerAddr, ownerEarnAmount);
            insertTx(msg.sender, _uplineAddr, uplineEarnAmount);
        } else {
            // Reached earn count threshold, upline join Plan B
            insertTx(msg.sender, _uplineAddr, uplineEarnAmount +  ownerEarnAmount);
            joinPlanB(_uplineAddr);
        }
    }

	function getPlayerBalanceAmount(address _playerAddr) public view returns (uint256) {
		return playerList[_playerAddr].balanceAmt;
	} 

	function getPlanBCurrentRecipient() public view returns (address) {
		return planBPlayerAddrList[planBRcp.indexPos];
	}

    function getPlanBPlayerAddressByIndexPosition(uint256 _indexPos) public view returns (address) {
        return planBPlayerAddrList[_indexPos];
    }

	function getPlayerByAddress(address _playerAddr) public view returns (Player memory) {
		return playerList[_playerAddr];
	}

    function getOwnerAddress() public view returns (address) {
        return ownerAddr;
    }

    function joinPlanB(address _playerAddr) private {
        // Init the plan B recipient if not yet init
        if(!planBRcp.isInit){
            planBRcp.isInit = true;
            planBRcp.indexPos = 0;
            planBRcp.earnCount = 0;
        }

        // Insert the player to plan B
        planBPlayerAddrList.push(_playerAddr);
        playerList[_playerAddr].planBCurrentIndexPos = planBPlayerAddrList.length - 1;

        // Process only if recipient is different than the player
        if(_playerAddr != planBPlayerAddrList[planBRcp.indexPos]){
            // Recipient still eligible to earn
            if(planBRcp.earnCount < earnCountThreshold){
                // Payout to recipient
                insertTx(_playerAddr, planBPlayerAddrList[planBRcp.indexPos], uplineEarnAmount);
                planBRcp.earnCount++;
                // Payout to  owner
                insertTx(_playerAddr, ownerAddr, ownerEarnAmount);
            } else {
                // Threshold reach, recipient get 100% payout
                address oldRcpAddr = planBPlayerAddrList[planBRcp.indexPos];
                insertTx(_playerAddr, oldRcpAddr, uplineEarnAmount + ownerEarnAmount);
                // Derive new recipient
                planBRcp.indexPos++;
                planBRcp.earnCount = 0;
                // Demote the old Recipient
                joinPlanB(oldRcpAddr);
            }
        }
    }

    function insertTx(address _from, address _to, uint256 _amount) private {
        PlayTransaction memory playTx;

        // Insert the transaction record
        playTx.txTime = block.timestamp;
        playTx.fromPlayer = _from;
        playTx.toPlayer = _to;
        playTx.amount = _amount;
        playTxList.push(playTx);
        uint256 newLength = playTxList.length;

        // Associate the transaction to related playerList
        playerList[_from].playTxIndexList.push(newLength - 1);
        playerList[_to].playTxIndexList.push(newLength - 1);

        // Update the toPlayer balance
        if(playerList[_to].isInit){
            playerList[_to].balanceAmt += _amount; 
        }
    }

}